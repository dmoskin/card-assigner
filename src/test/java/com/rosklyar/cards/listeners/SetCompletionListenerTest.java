package com.rosklyar.cards.listeners;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.util.AlbumControl;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by Dmytro Mospanenko.
 */
@RunWith(MockitoJUnitRunner.class)
public class SetCompletionListenerTest {

    @Mock
    private AlbumControl control;

    private final String TEST_SET = "TestSet";
    private final long TEST_USER_ID = 1L;

    @Test
    public void testOnEventFire() {
        when(control.isSetFull(TEST_SET, 1)).thenReturn(true);
        final MutableBoolean isTriggered = new MutableBoolean(false);
        SetCompletionListener listener = new SetCompletionListener(event -> {
            isTriggered.setTrue();
            assertThat(event).isNotNull();
            assertThat(event).isEqualTo(new Event(TEST_USER_ID, Event.Type.SET_FINISHED));
        }, control);

        listener.onEvent(
                TEST_USER_ID,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        TEST_SET,
                        "TestAlbum"
                )
        );

        assertThat(isTriggered.booleanValue()).isTrue();
    }

    @Test
    public void testOnEventWhenItsNotTimeToFire() {
        when(control.isSetFull(TEST_SET, 1)).thenReturn(false);
        List<Event> eventList = new ArrayList<>();

        SetCompletionListener listener = new SetCompletionListener(eventList::add, control);

        listener.onEvent(
                TEST_USER_ID,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        TEST_SET,
                        "TestAlbum"
                )
        );

        assertThat(eventList).isEmpty();
    }

    @Test
    public void testOnEventWhenUserNullOnEvent() {
        SetCompletionListener listener = new SetCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                null,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        TEST_SET,
                        "TestAlbum"
                )
        )).isInstanceOf(NullPointerException.class)
        .hasMessageContaining("User id is null");
    }

    @Test
    public void testOnEventWhenCardNullOnEvent() {
        SetCompletionListener listener = new SetCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                TEST_USER_ID,
                null
        )).isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Card is null");
    }

    @Test
    public void testOnEventWhenAllArgsNullOnEvent() {
        SetCompletionListener listener = new SetCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                null,
                null
        )).isInstanceOf(NullPointerException.class)
                .hasMessageContaining("User id is null");
    }

    public void testListenerCreationWithNullCallback() {
        assertThatThrownBy(() -> new SetCompletionListener(null, control))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Callback is null");
    }

    public void testListenerCreationWithNullControlAlbun() {
        assertThatThrownBy(() -> new SetCompletionListener(event -> {}, null))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Album control is null");
    }

}