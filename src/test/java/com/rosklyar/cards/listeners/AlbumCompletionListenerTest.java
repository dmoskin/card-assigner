package com.rosklyar.cards.listeners;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.util.AlbumControl;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by Dmytro Mospanenko.
 */
@RunWith(MockitoJUnitRunner.class)
public class AlbumCompletionListenerTest {

    @Mock
    private AlbumControl control;

    private final long TEST_USER_ID = 1L;

    @Test
    public void testOnEventFire() {
        when(control.isAlbumFull(1)).thenReturn(true);
        final MutableBoolean isTriggered = new MutableBoolean(false);

        AlbumCompletionListener listener = new AlbumCompletionListener(event -> {
            isTriggered.setTrue();
            assertThat(event).isNotNull();
            assertThat(event).isEqualTo(new Event(TEST_USER_ID, Event.Type.ALBUM_FINISHED));
        }, control);

        listener.onEvent(
                TEST_USER_ID,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        "TestSet",
                        "TestAlbum"
                )
        );

        assertThat(isTriggered.booleanValue()).isTrue();
    }

    @Test
    public void testOnEventWhenItsNotTimeToFire() {
        when(control.isAlbumFull(1)).thenReturn(false);
        final MutableBoolean isTriggered = new MutableBoolean(false);

        AlbumCompletionListener listener = new AlbumCompletionListener(event -> {
            isTriggered.setTrue();
        }, control);

        listener.onEvent(
                TEST_USER_ID,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        "TestSet",
                        "TestAlbum"
                )
        );

        assertThat(isTriggered.booleanValue()).isFalse();
    }

    @Test
    public void testOnEventWhenUserNullOnEvent() {
        AlbumCompletionListener listener = new AlbumCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                null,
                new UserCardHolder(
                        1L,
                        "TestCard",
                        "TestSet",
                        "TestAlbum"
                )
        )).isInstanceOf(NullPointerException.class)
                .hasMessageContaining("User id is null");
    }

    @Test
    public void testOnEventWhenCardNullOnEvent() {
        AlbumCompletionListener listener = new AlbumCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                TEST_USER_ID,
                null
        )).isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Card is null");
    }

    @Test
    public void testOnEventWhenAllArgsNullOnEvent() {
        AlbumCompletionListener listener = new AlbumCompletionListener(event -> {}, control);

        assertThatThrownBy(() -> listener.onEvent(
                null,
                null
        )).isInstanceOf(NullPointerException.class)
                .hasMessageContaining("User id is null");
    }

    public void testListenerCreationWithNullCallback() {
        assertThatThrownBy(() -> new AlbumCompletionListener(null, control))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Callback is null");
    }

    public void testListenerCreationWithNullControlAlbun() {
        assertThatThrownBy(() -> new AlbumCompletionListener(event -> {}, null))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Album control is null");
    }

}