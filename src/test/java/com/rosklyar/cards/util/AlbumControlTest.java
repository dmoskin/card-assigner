package com.rosklyar.cards.util;

import com.rosklyar.cards.domain.Album;
import com.rosklyar.cards.domain.AlbumSet;
import com.rosklyar.cards.domain.Card;
import com.rosklyar.cards.domain.UserCardHolder;
import org.junit.Before;
import org.junit.Test;

import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Created by Dmytro Mospanenko.
 */
public class AlbumControlTest {

    private Album testAlbum;
    private AlbumControl testControl;

    private final String TEST_ALBUM = "TestAlbum";
    private final String TEST_SET = "TestSet";
    private final String TEST_SET1 = "TestSet1";

    private final String CONGIG_ERR = "Album control is not configured";

    private final long TEST_CARD_ID = 1L;

    @Before
    public void initData() {
        testAlbum = new Album(1L, TEST_ALBUM,
                        newHashSet(
                            new AlbumSet(1L, TEST_SET,
                                newHashSet(
                                    new Card(TEST_CARD_ID, "Card1"),
                                    new Card(2L, "Card2")
                                )
                            ),
                            new AlbumSet(2L, TEST_SET1,
                                 newHashSet(
                                     new Card(3L, "Card1")
                                 )
                        )
                    ));
        testControl = new AlbumControl(testAlbum);
    }

    @Test
    public void testAlbumControlCreationWithNullArgs() {
        assertThatThrownBy(() -> new AlbumControl(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Album must not be null");
    }

    @Test
    public void testIsSetFullTestWhenUserDeckIsNotFull() {
        assertThat(testControl.isSetFull(TEST_SET, 1)).isFalse();
    }

    @Test
    public void testIsSetFullTestWhenUserDeckIsFull() {
        int fullSetSize  = testAlbum
                .sets
                    .stream()
                    .filter(albumSet -> albumSet.name.equals(TEST_SET))
                    .mapToInt(value -> value.cards.size())
                    .sum();

        assertThat(testControl.isSetFull(TEST_SET, fullSetSize)).isTrue();
    }

    @Test
    public void testIsSetFullTestWhenSizeIsBigNumber() {
        assertThat(testControl.isSetFull(TEST_SET, Integer.MAX_VALUE)).isFalse();
    }

    @Test
    public void testIsSetFullTestWhenThereIsNoSuchCardSet() {
        final String setName = "NONEXISTENT_SET";
        assertThatThrownBy(() -> testControl.isSetFull(setName, 1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("no set with name " + setName);
    }

    @Test
    public void testIsSetFullTestWithNullSetName() {
        assertThatThrownBy(() -> testControl.isSetFull(null, 1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("no set with name null");
    }

    @Test
    public void testIsSetFullTestWithZeroDeckSizeValue() {
        assertThatThrownBy(() -> testControl.isSetFull(TEST_SET, 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("larger than zero");
    }

    @Test
    public void testIsSetFullTestWithNegativeDeckSizeValue() {
        assertThatThrownBy(() -> testControl.isSetFull(TEST_SET, -1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("value must be positive");
    }

    @Test
    public void testIsSetFullTestWithBigNegativeDeckSizeValue() {
        assertThatThrownBy(() -> testControl.isSetFull(TEST_SET, Integer.MIN_VALUE))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("value must be positive");
    }

    @Test
    public void testIsAlbumFull() {
        int fullAlbumSize  = testAlbum
                .sets
                .stream()
                .mapToInt(value -> value.cards.size())
                .sum();
        assertThat(testControl.isAlbumFull(fullAlbumSize)).isTrue();
    }

    @Test
    public void testIsAlbumFullLargeSize() {
        assertThat(testControl.isAlbumFull(Integer.MAX_VALUE)).isFalse();
    }

    @Test
    public void testIsAlbumFullLargeNegativeSize() {
        assertThatThrownBy(() -> testControl.isAlbumFull(Integer.MIN_VALUE))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("must be positive value");
    }

    @Test
    public void testIsAlbumFullNegativeSize() {
        assertThatThrownBy(() -> testControl.isAlbumFull(-1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("must be positive value");
    }

    @Test
    public void testIsAlbumFullWithZeroSize() {
        assertThatThrownBy(() -> testControl.isAlbumFull(0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("larger than zero");
    }

    @Test
    public void testGetCardHolderByCardId() {
        Card cardFromSet = testAlbum
                .sets
                .stream()
                .filter(albumSet -> albumSet.name.equals(TEST_SET))
                .findFirst()
                .get()
                .cards
                .stream()
                .filter(card -> card.id == TEST_CARD_ID)
                .findFirst()
                .get();
        UserCardHolder testedCard = testControl.getCardHolderByCardId(TEST_CARD_ID);
        assertThat(testedCard.cardId).isEqualTo(cardFromSet.id);
        assertThat(testedCard.cardName).isEqualTo(cardFromSet.name);
        assertThat(testedCard.setName).isEqualTo(TEST_SET);
        assertThat(testedCard.albumName).isEqualTo(TEST_ALBUM);
    }

    @Test
    public void testGetCardHolderByCardIdWithNonexistentId() {
        assertThatThrownBy(() -> testControl.getCardHolderByCardId(Long.MAX_VALUE))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("There is no such");
    }
}