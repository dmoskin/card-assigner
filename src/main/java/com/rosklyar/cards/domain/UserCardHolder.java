package com.rosklyar.cards.domain;

import java.util.Objects;

/**
 * Created by Dmytro Mospanenko.
 */
public class UserCardHolder {
    public final Long cardId;
    public final String cardName;
    public final String setName;
    public final String albumName;

    public UserCardHolder(Long cardId, String cardName, String setName, String albumName) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.setName = setName;
        this.albumName = albumName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCardHolder that = (UserCardHolder) o;
        return Objects.equals(cardId, that.cardId) &&
                Objects.equals(cardName, that.cardName) &&
                Objects.equals(setName, that.setName) &&
                Objects.equals(albumName, that.albumName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId, cardName, setName, albumName);
    }
}
