package com.rosklyar.cards.service;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.listeners.AlbumCompletionListener;
import com.rosklyar.cards.listeners.SetCompletionListener;
import com.rosklyar.cards.user.User;
import com.rosklyar.cards.util.AlbumControl;

import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created by rostyslavs on 11/21/2015.
 *
 * Decided to implement this task with observer pattern and listeners which works on callbacks
 */
public class DefaultCardAssigner implements CardAssigner {

    private Consumer<Event> callback;

    private Map<Long, User> userMap;
    private AlbumControl control;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-1s] %5$s %n");
    }

    public DefaultCardAssigner(ConfigurationProvider configurationProvider) {
        Objects.requireNonNull(configurationProvider, "Injected configuration provider must not be null");
        Objects.requireNonNull(configurationProvider.get(), "Configuration provider doesn't have album description, it's null");

        control = new AlbumControl(configurationProvider.get());
        userMap = new Hashtable<>();
    }

    @Override
    public void assignCard(long userId, long cardId) {
        User user = userMap.computeIfAbsent(userId, k -> {
            User newUser =  new User(userId);
            newUser.registerEventListener(new SetCompletionListener(callback, control));
            newUser.registerEventListener(new AlbumCompletionListener(callback, control));
            return newUser;
        });
        user.takeCard(control.getCardHolderByCardId(cardId));
    }

    @Override
    public void subscribe(Consumer<Event> consumer) {
        Objects.requireNonNull(consumer, "Unable to subscribe. Callback is null");
        callback = consumer;
    }
}
