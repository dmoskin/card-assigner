package com.rosklyar.cards.listeners;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.util.AlbumControl;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created by Dmytro Mospanenko
 * This is abstract observer
 */
public abstract class AbstractBasicListener {

    Consumer<Event> consumer;
    AlbumControl control;

    AbstractBasicListener(Consumer<Event> consumer, AlbumControl control) {
        Objects.requireNonNull(consumer, "Callback is null");
        Objects.requireNonNull(control, "Album control is null");
        this.consumer = consumer;
        this.control = control;
    }

    public abstract void onEvent(Long userId, UserCardHolder cardHolder);

    void validate(Long userId, UserCardHolder cardHolder) {
        Objects.requireNonNull(userId, "User id is null");
        Objects.requireNonNull(cardHolder, "Card is null");
    }
}
