package com.rosklyar.cards.listeners;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.util.AlbumControl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * Created by Dmytro Mospanenko
 */
public class SetCompletionListener extends AbstractBasicListener {
    private final Logger log = Logger.getLogger(SetCompletionListener.class.getName());

    private final Map<String, Integer> setSizesMap = Collections.synchronizedMap(new HashMap<>());

    public SetCompletionListener(Consumer<Event> consumer, AlbumControl control) {
        super(consumer, control);
    }

    @Override
    public void onEvent(Long userId, UserCardHolder cardHolder) {
        validate(userId, cardHolder);
        setSizesMap.merge(cardHolder.setName, 1, Integer::sum);
        if(control.isSetFull(cardHolder.setName, setSizesMap.get(cardHolder.setName))) {
            consumer.accept(new Event(userId, Event.Type.SET_FINISHED));
            log.info(String.format("user: %d; event: %s", userId, Event.Type.SET_FINISHED));
        }
    }
}
