package com.rosklyar.cards.listeners;

import com.rosklyar.cards.domain.Event;
import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.util.AlbumControl;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * Created by Dmytro Mospanenko
 */
public class AlbumCompletionListener extends AbstractBasicListener {
    private final Logger log = Logger.getLogger(AlbumCompletionListener.class.getName());
    private final AtomicInteger albumSize = new AtomicInteger(0);

    public AlbumCompletionListener(Consumer<Event> consumer, AlbumControl control) {
        super(consumer, control);
    }

    @Override
    public void onEvent(Long userId, UserCardHolder cardHolder) {
        validate(userId, cardHolder);
        if(control.isAlbumFull(albumSize.incrementAndGet())) {
            consumer.accept(new Event(userId, Event.Type.ALBUM_FINISHED));
            log.info(String.format("user: %d; event: %s", userId, Event.Type.ALBUM_FINISHED));
        }
    }
}
