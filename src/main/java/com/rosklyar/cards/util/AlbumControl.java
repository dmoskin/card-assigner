package com.rosklyar.cards.util;

import com.rosklyar.cards.domain.Album;
import com.rosklyar.cards.domain.UserCardHolder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Dmytro Mospanenko.
 */
public class AlbumControl {

    private Map<String, Integer> controlSetSizesMap;
    private Map<Long, UserCardHolder> controlDeck;

    public AlbumControl(Album album){
        prepareControlDeck(album);
        prepareSetSizesMap(album);
    }

    public boolean isSetFull(String setName, int userSetSize) {
        if(controlSetSizesMap.get(setName) == null) {
            throw new IllegalArgumentException("There is no set with name " + setName);
        }

        if(userSetSize <= 0) {
            throw new IllegalArgumentException("Set size value must be positive value larger than zero.");
        }
        return controlSetSizesMap.get(setName) == userSetSize;
    }

    public boolean isAlbumFull(int userCardHolderSize) {
        if(userCardHolderSize <= 0) {
            throw new IllegalArgumentException("Card deck size value must be positive value larger than zero");
        }

        return controlDeck.size() == userCardHolderSize;
    }

    public UserCardHolder getCardHolderByCardId(long cardId) {
        UserCardHolder result = controlDeck.get(cardId);
        if(result == null) {
            throw new IllegalStateException("There is no such card");
        }
        return result;
    }

    /**
     * prepare more flat data structure for faster and easier search
     * @param album
     */
    private void prepareControlDeck(Album album) {
        Objects.requireNonNull(album, "Album must not be null");
        controlDeck = new HashMap<>();
        album
             .sets
                .forEach(albumSet -> {
                    albumSet.cards.forEach(card -> {
                        controlDeck.put(
                                card.id,
                                new UserCardHolder(
                                     card.id,
                                     card.name,
                                     albumSet.name,
                                     album.name
                                )
                        );
                    });
             });

    }

    /**
     * Its better to know about sets sizes than iterate through Album data structure
     * every time when we need to check does user have full set in his deck of cards.
     * @param album
     */
    private void prepareSetSizesMap(Album album) {
        Objects.requireNonNull(album, "Album must not be null");
        controlSetSizesMap = new HashMap<>();
        album
             .sets
             .forEach(albumSet -> controlSetSizesMap.put(albumSet.name, albumSet.cards.size()));
    }

}
