package com.rosklyar.cards.user;

import com.rosklyar.cards.domain.UserCardHolder;
import com.rosklyar.cards.listeners.AbstractBasicListener;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Dmytro Mospanenko.
 * This class represents subject
 */
public class User {
    private Long id;
    private Set<AbstractBasicListener> eventListenerList;
    private Set<UserCardHolder> userCardDeck;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();


    public User(Long id) {
        this.id = id;
        eventListenerList = new HashSet<>();
        userCardDeck = new HashSet<>();
    }

    public void takeCard(UserCardHolder cardHolder) {
        this.writeLock.lock();
        try {
            Objects.requireNonNull(cardHolder);
            if (userCardDeck.add(cardHolder)) {
                fireEvents(cardHolder);
            }
        } finally {
            this.writeLock.unlock();
        }
    }

    public void registerEventListener(AbstractBasicListener listener) {
        this.writeLock.lock();
        try {
            eventListenerList.add(listener);
        } finally {
            this.writeLock.unlock();
        }
    }

    public void removeEventListener(AbstractBasicListener listener) {
        this.writeLock.lock();
        try {
            eventListenerList.remove(listener);
        } finally {
            this.writeLock.unlock();
        }
    }

    private void fireEvents(UserCardHolder cardHolder) {
        this.readLock.lock();
        try {
            this.eventListenerList.forEach(listener -> listener.onEvent(id, cardHolder));
        } finally {
            this.readLock.unlock();
        }
    }
}
